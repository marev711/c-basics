#include <stdio.h>

int main(int numArgs, char* args[]) {
    char* name;
    if (numArgs == 1) {
        name = "Dennis Ritchie";
    } else {
        name = args[1];
    }
    printf("Hello World, from %s\n", name);
    return 0;
}
