cmake_minimum_required(VERSION 3.8)
project(02_scalar_types)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_FLAGS "-Wall -Wextra")

add_executable(values values.c)
add_executable(sizes sizes.c)
