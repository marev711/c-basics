#include <stdio.h>

typedef enum {
    CHECK=10, CREDIT=20
} AccountType;

typedef struct {
    AccountType type;
    int         balance;
    float       rate;
} Account;

int main() {
    {
        Account a1 = {CHECK, 1000, 1.75};
        Account a2 = {CREDIT, 250, 2.25};
        printf("a1 = {%d, %d, %.2f%%}\n", a1.type, a1.balance, a1.rate);
        printf("a2 = {%d, %d, %.2f%%}\n", a2.type, a2.balance, a2.rate);
    }
    printf("------\n");
    {
        Account accounts[] = {
                {CHECK,  100, 1.0},
                {CHECK,  200, 1.5},
                {CREDIT, 300, 2.0},
                {CHECK,  400, 2.5},
                {CREDIT, 500, 3.0},
        };
        int idx = 0;
        printf("accounts[%d] = {%d, %d, %.2f%%} --> %d\n",
               idx,
               accounts[idx].type,
               accounts[idx].balance,
               accounts[idx].rate,
               (int) (accounts[idx].balance * (1 + accounts[idx].rate / 100))
        );

        ++idx;
        printf("accounts[%d] = {%d, %d, %.2f%%} --> %d\n",
               idx,
               accounts[idx].type,
               accounts[idx].balance,
               accounts[idx].rate,
               (int) (accounts[idx].balance * (1 + accounts[idx].rate / 100))
        );

        ++idx;
        printf("accounts[%d] = {%d, %d, %.2f%%} --> %d\n",
               idx,
               accounts[idx].type,
               accounts[idx].balance,
               accounts[idx].rate,
               (int) (accounts[idx].balance * (1 + accounts[idx].rate / 100))
        );

        ++idx;
        printf("accounts[%d] = {%d, %d, %.2f%%} --> %d\n",
               idx,
               accounts[idx].type,
               accounts[idx].balance,
               accounts[idx].rate,
               (int) (accounts[idx].balance * (1 + accounts[idx].rate / 100))
        );

        ++idx;
        printf("accounts[%d] = {%d, %d, %.2f%%} --> %d\n",
               idx,
               accounts[idx].type,
               accounts[idx].balance,
               accounts[idx].rate,
               (int) (accounts[idx].balance * (1 + accounts[idx].rate / 100))
        );
    }

    return 0;
}