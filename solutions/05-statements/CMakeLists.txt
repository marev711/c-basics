cmake_minimum_required(VERSION 3.8)
project(05_statements)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_FLAGS "-Wall -Wextra")

add_executable(accounts accounts.c)
