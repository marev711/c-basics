#include <stdio.h>

typedef enum {
    CHECK = 10, CREDIT = 20
} AccountType;

typedef float InterestRate;

typedef struct {
    AccountType type;
    int balance;
    InterestRate rate;
} Account;


int main() {
    InterestRate rates[] = {1.2, 2.4, 3.33, 1.75};
    const int R = sizeof(rates) / sizeof(rates[0]);

    const int N = 7;
    Account accounts[N];

    for (int k = 0; k < N; ++k) {
        accounts[k].type = (k % 2) ? CHECK : CREDIT;
        accounts[k].balance = (k + 1) * 1000;
        accounts[k].rate = rates[k % R];
    }

    for (int k = 0; k < N; ++k) {
        printf("accounts[%02d]: {%s, SEK %d, %.2f%%}\n",
               k,
               accounts[k].type == CHECK ? "CHECK " : "CREDIT",
               accounts[k].balance,
               accounts[k].rate);
    }

    for (int k = 0; k < N; ++k) {
        accounts[k].balance *= 1 + accounts[k].rate / 100;
        if (accounts[k].balance > 6000) accounts[k].balance = 5000;
    }

    printf("-------\n");
    for (int k = 0; k < N; ++k) {
        printf("accounts[%02d]: {%s, SEK %d, %.2f%%}\n",
               k,
               accounts[k].type == CHECK ? "CHECK " : "CREDIT",
               accounts[k].balance,
               accounts[k].rate);
    }

    return 0;
}
