//
// Created by jensr on 2018-04-16.
//
#include <stdio.h>
#include "Account.h"



void accounts_init(Account accounts[], const unsigned int size) {
    const float    rate[] = {0.75, 1.25, 2.15};
    const unsigned R      = sizeof(rate) / sizeof(rate[0]);
    for (unsigned  k      = 0; k < size; ++k) {
        account_init(&accounts[k], k % 2, (k + 1) * 100, rate[k % R]);
    }
}

void accounts_print(Account* accounts, const unsigned int size) {
    for (unsigned k = 0; k < size; ++k) {
        printf("[%d] {%s, SEK %d, %.2f%%}\n",
               k + 1,
               accounts[k].type == CHECK ? "CHECK " : "CREDIT",
               accounts[k].balance,
               accounts[k].rate
        );
    }
}

void accounts_update(Account accounts[], const unsigned int size) {
    for (unsigned k = 0; k < size; ++k) {
        const float p = accounts[k].rate;
        accounts[k].balance *= (1 + p / 100);
        if (accounts[k].balance > 6000) accounts[k].balance = 5000;
    }
}

Account* account_init(Account* acc, AccountType type, int balance, float rate) {
    acc->type    = type;
    acc->balance = balance;
    acc->rate    = rate;
    return acc;
}
