
#include <stdio.h>
#include "account.h"


char* account_type(AccountType type) {
    switch (type) {
        case CHECK:
            return "CHECK ";
        case CREDIT:
            return "CREDIT";
    }
    return "**INVALID**";
}

void account_init(Account* this, AccountType type, Balance amount, InterestRate percentage) {
    this->type    = type;
    this->balance = amount;
    this->rate    = percentage;
}

void account_print(Account* this) {
    printf("account{%s, SEK %d, %.2f%%}\n",
           account_type(this->type), this->balance, this->rate);
}

Balance account_amount(Account* this) {
    return account_final_amount(this->balance, this->rate);
}


Balance account_final_amount(Balance a, InterestRate p) {
    return (Balance)(a * (1 + p / 100));
}

void accounts_init(Account* accounts, int numAccounts) {
    static InterestRate   rates[] = {1.5, 2, 2.5, 3};
    static const unsigned R       = sizeof(rates) / sizeof(InterestRate);

    for (int k = 0; k < numAccounts; ++k) {
        account_init(&accounts[k], k % 2, (k + 1) * 1000, rates[k % R]);
    }
}

void accounts_print(Account* accounts, int numAccounts) {
    for (int k = 0; k < numAccounts; ++k) {
        account_print(&accounts[k]);
    }
}

