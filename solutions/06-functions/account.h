#pragma once

typedef enum {
    CHECK, CREDIT
} AccountType;

typedef int   Balance;
typedef float InterestRate;

typedef struct {
    AccountType  type;
    Balance      balance;
    InterestRate rate;
} Account;

// Using 'extern' is not strictly needed for functions and modern compilers,
// but recommended anyway because of clarity
extern char*    account_type(AccountType);
extern void     account_init(Account*, AccountType, Balance, InterestRate);
extern void     account_print(Account*);
extern Balance  account_amount(Account*);
extern Balance  account_final_amount(Balance, InterestRate);

extern void     accounts_init(Account[], int);
extern void     accounts_print(Account[], int);
