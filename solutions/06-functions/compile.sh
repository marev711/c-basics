#!/usr/bin/env bash
set -e

SRCS=.
OBJS=./objs
EXE=bank
CFLAGS="-std=c99 -Wall -Wextra -Werror -Wfatal-errors"

set -x
rm -rf ${OBJS}
mkdir -p ${OBJS}

gcc ${CFLAGS} ${SRCS}/account.c -c -o ${OBJS}/account.o
gcc ${CFLAGS} ${SRCS}/bank.c    -c -o ${OBJS}/bank.o

gcc ${OBJS}/account.o ${OBJS}/bank.o -o ${EXE}
