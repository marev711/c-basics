#include <stdio.h>
#include "defs.h"
#include "defs.h"
#include "defs.h"

int main() {
    int a = 42, b = 17;

    printf("min(%d, %d) = %d\n", a, b, min(a, b));
    printf("max(%d, %d) = %d\n", a, b, max(a, b));

    return 0;
}
