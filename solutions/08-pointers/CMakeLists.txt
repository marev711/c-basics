cmake_minimum_required(VERSION 3.8)
project(08_pointers)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "-Wall -Wextra ")

add_executable(accounts account.h account.c bank.c)

add_executable(indirect-int indirect-int.c)
add_executable(indirect-array indirect-array.c)
add_executable(single-account single-account.c)
add_executable(array-account array-account.c)
add_executable(account-functions account-functions.c)
add_executable(interest-computation interest-computation.c)
