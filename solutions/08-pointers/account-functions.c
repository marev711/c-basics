#include <stdio.h>


typedef struct {
    int   balance;
    float rate;
} Account;

void account_print(Account* self);
Account* account_init(Account* self, int balance, float rate);


int main() {
    Account acc;
    account_init(&acc, 500, 1.25);
    account_print(&acc);

    printf("----------\n");
    Account arr[10];
    const unsigned N = sizeof(arr) / sizeof(arr[0]);
    for (unsigned k = 0; k < N; ++k) {
        account_init(&arr[k], (k + 1) * 100, (float) ((k + 1) * 0.25));
    }
    for (unsigned k = 0; k < N; ++k) {
        account_print(&arr[k]);
    }

    return 0;
}


Account* account_init(Account* self, int balance, float rate) {
    self->balance = balance;
    self->rate = rate;
    return self;
}

void account_print(Account* self) {
    printf("Account{SEK %d, %.2f%%}\n", self->balance, self->rate);
}
