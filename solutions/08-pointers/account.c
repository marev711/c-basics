#include <stdio.h>
#include "account.h"


static char* account_type(AccountType type) {
    switch (type) {
        case CHECK:
            return "CHECK ";
        case CREDIT:
            return "CREDIT";
    }
    return "**INVALID**";
}

Account* account_init(Account* this, AccountType type, Balance amount, InterestRate percentage) {
    this->type = type;
    this->balance = amount;
    this->rate = percentage;
    return this;
}

Account* account_print(Account* this) {
    printf("account{%s, SEK %d, %.2f%%}\n",
           account_type(this->type), this->balance, this->rate);
    return this;
}

Balance account_final_amount(Account* this) {
    const Balance a = this->balance;
    const InterestRate p = this->rate;
    return (Balance) (a * (1 + p / 100));
}

Balance account_initial_amount(Account* this) {
    const Balance a = this->balance;
    const InterestRate p = this->rate;
    return (Balance) (a / (1 + p / 100));
}
