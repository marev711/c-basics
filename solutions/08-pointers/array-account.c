#include <stdio.h>

typedef struct {
    int   balance;
    float rate;
} Account;

int main() {
    Account        arr[] = {
            {100, 0.25},
            {200, 0.5},
            {300, 0.75},
            {400, 1.0},
            {500, 1.25},
            {600, 1.55},
    };
    const unsigned N     = sizeof(arr) / sizeof(arr[0]);
    const Account* END = &arr[N];

    for (Account* p = &arr[0]; p != END; ++p) {
        printf("acc: {SEK %d, %.2f%%}\n", p->balance, p->rate);
    }

    for (Account* p = (arr + 0); p != END; ++p) {
        p->balance *= (1 + p->rate / 100);
    }

    printf("----------\n");
    for (Account* p = arr; p != END; ++p) {
        printf("acc: {SEK %d, %.2f%%}\n", p->balance, p->rate);
    }

    return 0;
}
