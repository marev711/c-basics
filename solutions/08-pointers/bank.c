#include <stdio.h>
#include "account.h"

typedef Balance (* BalanceFunc)(Account*);

Account* readAccount(Account* acc) {
    printf("Enter amount: ");
    Balance a;
    scanf("%d", &a);

    printf("Enter rate: ");
    InterestRate p;
    scanf("%f", &p);

    return account_init(acc, CHECK, a, p);
}

void compute(Account* acc) {
    BalanceFunc funcs[] = {
            &account_final_amount,
            &account_initial_amount,
    };
    const unsigned F = sizeof(funcs) / sizeof(BalanceFunc);

    printf("--- Funcs ---\n");
    for (unsigned k = 0; k < F; ++k) {
        acc->balance = (*funcs[k])(acc);
        account_print(acc);
    }
}

int main() {
    Account acc;
    account_print(readAccount(&acc));

    compute(&acc);
    return 0;
}
