#include <stdio.h>

int main() {
    int number = 42;
    printf("number = %d\n", number);

    int* p = &number;
    printf("p = %ld\n", (unsigned long)p);
    printf("p = %p (%d)\n", p, *p);

    *p *= 10;
    printf("number = %d (%d)\n", number, *p);

    return 0;
}

