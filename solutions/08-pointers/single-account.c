#include <stdio.h>

typedef struct {
    int   balance;
    float rate;
} Account;

int main() {
    Account acc = {1000, 1.5};
    printf("acc: {SEK %d, %.2f%%}\n", acc.balance, acc.rate);

    Account* p = &acc;
    printf("acc: {SEK %d, %.2f%%}\n", (*p).balance, p->rate);

    p->balance *= 10;
    printf("acc: {SEK %d, %.2f%%}\n", (*p).balance, p->rate);

    return 0;
}
