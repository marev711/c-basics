#pragma once

typedef enum {
    CHECK, CREDIT
} AccountType;

typedef int Balance;
typedef float InterestRate;

typedef struct {
    AccountType type;
    Balance balance;
    InterestRate rate;
} Account;


extern Account*     account_new(Balance amount, InterestRate percentage);
extern void         account_dispose(Account* this);
extern Account*     account_print(Account* this);
extern Balance      account_initial_amount(Account*);
extern Balance      account_final_amount(Account*);
