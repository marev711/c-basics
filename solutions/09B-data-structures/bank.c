#include <stdio.h>
#include <assert.h>
#include "Account.h"
#include "List.h"

void populate(List* accounts, int numAccounts) {
    InterestRate   rates[] = {0.5, 1.5, 2, 2.5, 3};
    const unsigned R       = sizeof(rates) / sizeof(rates[0]);

    while (--numAccounts >= 0) {
        Balance      amount     = 50 + (numAccounts * 100) % 10000;
        InterestRate percentage = rates[numAccounts % R];

        list_push(accounts, account_new(amount, percentage));
    }
}

void print(List* accounts) {
    int accno = 1;
    for (Node* node = accounts->first; node != NULL; node = node->next) {
        printf("%4d) ", accno++);
        account_print(node->data);
    }
}

typedef Balance (* AccountTransformer)(Account*);

void apply(List* accounts, AccountTransformer f) {
    for (Node* node = accounts->first; node != NULL; node = node->next) {
        (*f)(node->data);
    }
}

void drain(List* accounts) {
    while (!list_empty(accounts)) {
        account_dispose(list_pop(accounts));
    }
}

int main(int numArgs, char* args[]) {
    int numAccounts = (numArgs > 1) ? atoi(args[1]) : 5;

    List accounts;
    list_init(&accounts);
    populate(&accounts, numAccounts);
    assert(list_size(&accounts) == numAccounts);
    print(&accounts);

    printf("------------\n");
    apply(&accounts, &account_final_amount);
    print(&accounts);

    printf("------------\n");
    apply(&accounts, &account_initial_amount);
    print(&accounts);

    drain(&accounts);
    assert(list_empty(&accounts));

    Account* ptr = account_new(100, 1.5);
    account_print(ptr);
    account_dispose(ptr);
    return 0;
}
