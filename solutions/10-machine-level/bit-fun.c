#include <stdio.h>
#include <assert.h>

int main() {
    unsigned x = 17;
    printf("x = %d\n", x);

    printf("2*x:%d == x<<1:%d\n", 2 * x, x << 1);
    assert(2 * x == x << 1);

    printf("2*2*2*x:%d == x<<3:%d\n", 2 * 2 * 2 * x, x << 3);
    assert(2 * 2 * 2 * x == x << 3);

    printf("%d == %d\n", 32 / 8, 32 >> 3);
    assert((32 / 8) == (32 >> 3));

    printf("42^x:%d\n", 42 ^ x);
    printf("59^42:%d\n", 59 ^ 42);
    printf("42^x^42:%d\n", 42 ^ x ^ 42);
    assert((42 ^ x ^ 42) == x);

    typedef struct {
        unsigned char op:3;
        unsigned char flags:2;
        unsigned char padding:3;
    } Data;

    unsigned d = 0b01110111;  // (d & 0x07) << 2, ((d >> 3) & 0x03)
    Data* pkg = (Data*) &d;

    printf("pkg = {op:%d, flags:%d, pad:%d}\n",
           pkg->op, pkg->flags, pkg->padding);
    assert(pkg->op == 7);
    assert(pkg->flags == 2);
    assert(pkg->padding == 3);

    return 0;
}
