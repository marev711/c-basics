//compile: gcc -std=c11 -Wall -Wextra math-stuff.c -o math-stuff -lm

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void mathStuff(float x) {
    printf("--- Math Stuff x:%g ---\n", x);
    printf("sqrt(%g)  = %g\n", x, sqrt(x));
    printf("log10(%g) = %g\n", x, log10(x));
    printf("sin(%g)   = %g\n", x, sin(x));
    printf("cos(%g)   = %g\n", x, cos(x));
}

int main(int numArgs, char* args[]) {
    if (numArgs == 1) {
        const int      numbers[] = {1, 2, 5, 10, 20, 42, 50};
        const unsigned N         = sizeof(numbers) / sizeof(numbers[0]);

        for (unsigned k = 0; k < N; ++k) mathStuff(numbers[k]);
    } else {
        for (int k = 1; k < numArgs; ++k) {
            const char* arg = args[k];
            mathStuff(atof(arg));
        }
    }
    return 0;
}
