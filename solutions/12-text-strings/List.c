

#include <stdlib.h>
#include "List.h"


static Node* node_alloc() {
    return (Node*) calloc(1, sizeof(Node));
}

static void node_dealloc(Node* this) {
    free((void*) this);
}

static Node* node_init(Node* this, void* payload, Node* next) {
    this->payload = payload;
    this->next = next;
    return this;
}

static Node* node_finit(Node* this) {
    //dispose payload ??
    return this;
}

static Node* node_new(void* payload, Node* next) {
    return node_init(node_alloc(), payload, next);
}

static void node_delete(Node* this) {
    node_dealloc(node_finit(this));
}


static List* list_alloc() {
    return (List*) calloc(1, sizeof(List));
}

static void list_dealloc(List* this) {
    free(this);
}

static List* list_init(List* this) {
    this->first = NULL;
    return this;
}

static List* list_finit(List* this) {
    // todo: dispose the list payload and node elems
    return this;
}

List* list_new() {
    return list_init(list_alloc());
}

void list_delete(List* this) {
    list_dealloc(list_finit(this));
}


List* list_push(List* this, void* payload) {
    this->first = node_new(payload, this->first);
    return this;
}

void* list_pop(List* this) {
    if (list_empty(this)) return NULL;

    Node* n = this->first;
    this->first = n->next;
    void* payload = n->payload;
    node_delete(n);

    return payload;
}

bool list_empty(List* this) {
    return this->first == NULL;
}

Node* list_first(List* this) {
    return this->first;
}