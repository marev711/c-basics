#pragma  once

typedef struct sNode {
    void* payload;
    struct sNode* next;
} Node;

typedef struct {
    Node* first;
} List;

typedef int bool;


extern List* list_new();

extern void list_delete(List*);

extern List* list_push(List*, void*);

extern void* list_pop(List*);

extern bool list_empty(List*);

extern Node* list_first(List*);


