
#include <stdio.h>
#include <stdlib.h>
#include "Person.h"
#include "List.h"


void dynamic(unsigned numPersons) {
    List* persons = list_new();

    for (unsigned k = 0; k < numPersons; ++k) {
        list_push(persons, person_new("nisse", (k + 1) * 10 % 80 + 15));
    }

    for (Node* n = list_first(persons); n != NULL; n = n->next) {
        printf("[dynamic] person = %s\n", person_toString((Person*) n->payload));
    }

    while (!list_empty(persons)) {
        Person* p = list_pop(persons);
        person_delete(p);
    }

    list_delete(persons);
}

void create() {                                             // --- C++ ---
    Person* p = person_new("Justin Time", 37);              // Person* p = new Person{"Justin Time", 37};
    printf("[create] person = %s\n", person_toString(p));   // cout << p->toString() << endl;

    person_setName(p, "Cris P. Bacon");                     // p->setName("...");
    printf("[create] person = %s\n", person_toString(p));   // cout << p->toString() << endl;

    person_setAge(p, 42);                                   // p->setAge(...);
    printf("[create] person = %s\n", person_toString(p));   // cout << p->toString() << endl;

    person_delete(p);                                       // delete p;
}


int main(int numArgs, char** args) {
    {
        printf("--- single person ---\n");
        Person p;
        person_init(&p, "Anna Conda", 42);
        printf("person = %s\n", person_toString(&p));
        person_finit(&p);
    }

    printf("--- dynamic person ---\n");
    create();

    printf("--- many persons ---\n");
    dynamic(numArgs == 1 ? 10 : (unsigned)atoi(args[1]));

    return 0;
}
