#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char* toString(char* name) {
    static char buf[64];
    sprintf(buf, "Hello %s", name);
    return buf;
}

char* toString2(char* name) {
    char* buf = (char*) calloc(strlen(name) + 1, sizeof(char));
    sprintf(buf, "Hello %s", name);
    return buf;
}


int main() {
    char* s = toString("Nisse");
    printf("s = '%s'\n", s);
    printf("s = '%s'\n", toString("Anna"));

    char* arr[] = {
            toString2("anna"),
            toString2("bea"),
            toString2("cissi"),
    };
    for (int k = 0; k < 3; ++k)
        printf("arr[%d]: %s\n", k, arr[k]);
    for (int k = 0; k < 3; ++k)
        free(arr[k]);

    printf("----------\n");
    char* arr0[] = {
            toString("anna"),
            toString("bea"),
            toString("cissi"),
    };
    for (int k = 0; k < 3; ++k)
        printf("arr[%d]: %s\n", k, arr0[k]);

    return 0;
}
