#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "string-lib.h"

#define TEST(func, payload, expected) { \
        char* actual = func(payload); \
        printf("%s(%s) -> [%s]\n", #func, payload, actual); \
        if (strcmp(actual, expected) != 0) { \
            printf("*** Failure: expected [%s]\n", expected); \
            printf("%s:%d\n", __FILE__, __LINE__); \
            exit(1); \
        } \
        free(actual); \
    }

int main() {
    const char* sample = "Hello World 123!";
    TEST(toUpperCase, sample, "HELLO WORLD 123!");
    TEST(toLowerCase, sample, "hello world 123!");
    TEST(reverse,     sample, "!321 dlroW olleH");
    TEST(reverse2,    sample, "!321 dlroW olleH");
    TEST(reverse2,    "abc", "cba");
    TEST(reverse2,    "abcd", "dcba");

    printf("all tests succeeded\n");
    return 0;
}
