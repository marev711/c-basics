#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "string-lib.h"

static const int EOS = '\0';

char* copy(const char* s) {
    return strcpy(calloc(strlen(s) + 1, sizeof(char)), s);
}

char* toUpperCase(const char* s) {
    char* result = copy(s);

    for (char* ch = result; *ch != EOS; ++ch) *ch = (char) toupper(*ch);
    return result;
}

char* toLowerCase(const char* s) {
    char* result = copy(s);

    for (char* ch = result; *ch != EOS; ++ch) *ch = (char) tolower(*ch);
    return result;
}

char* reverse(const char* s) {
    const unsigned LEN = (const unsigned) strlen(s);
    char* result = calloc(LEN + 1, sizeof(char));

    for (unsigned k = 0; k < LEN; ++k) result[k] = s[LEN - k - 1];
    return result;
}

static void swap(char* p, char* q) {
    char t = *p;
    *p = *q;
    *q = t;
}

char* reverse2(const char* s) {
    char* result = copy(s);
    char* p      = result;
    char* q      = result + strlen(result) - 1;
    for (; p < q; ++p, --q) swap(p, q);
    return result;
}

