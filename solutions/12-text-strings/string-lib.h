#pragma once
#include <string.h>

extern char* copy(const char*);
extern char* toUpperCase(const char*);
extern char* toLowerCase(const char*);
extern char* reverse(const char*);
extern char* reverse2(const char*);

#define streq(lhs, rhs)     (strcmp(lhs, rhs) == 0)
