#pragma once


typedef struct {
    unsigned numChars;
    unsigned numWords;
    unsigned numLines;
    const char* name;
} Count;

Count* cnt_init(Count* this, const char* name);
Count* cnt_add(Count* this, char* line);
Count* cnt_sum(Count* this, Count* that);
Count* cnt_print(Count* this);
