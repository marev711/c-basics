#include <stdio.h>
#include <string.h>

unsigned count_phrase(const char* phrase, const char* line) {
    const unsigned LEN = (const unsigned int) strlen(phrase);
    unsigned       n   = 0;
    for (const char* p = line; (p = strstr(p, phrase)) != NULL; p += LEN + 1) {
        ++n;
    }
    return n;
}

unsigned count(const char* phrase, FILE* file) {
    unsigned       n      = 0;
    const unsigned MAXLEN = 1024;
    char           line[MAXLEN];
    while (fgets(line, MAXLEN, file) != NULL) {
        n += count_phrase(phrase, line);
    }

    return n;
}

int main(int numArgs, char* args[]) {
    char* filename = "shakespeare.txt";
    char* phrase   = "Hamlet";

    for (int k = 1; k < numArgs; ++k) {
        char* arg = args[k];
        if (strcmp(arg, "-f") == 0) {
            filename = args[++k];
        } else if (strcmp(arg, "-p") == 0) {
            phrase = args[++k];
        } else {
            fprintf(stderr, "usage: %s [-p <phrase>] [-f <file>]\n", args[0]);
            return 1;
        }
    }

    FILE* f = fopen(filename, "r");
    if (!f) {
        fprintf(stderr, "cannot open '%s'\n", filename);
        return 1;
    }

    unsigned cnt = count(phrase, f);
    printf("Phrase '%s' occurs %d times in file '%s'\n",
           phrase, cnt, filename
    );

    return 0;
}
