#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Count.h"

Count* processInput(Count* this, FILE* f);
void processStdin();
void processFile(char* filename, Count* total);
void processFiles(int numArgs, char* args[]);


int main(int numArgs, char* args[]) {
    if (numArgs == 1) {
        processStdin();
    } else {
        processFiles(numArgs, args);
    }
    return 0;
}


void processStdin() {
    Count cnt;
    cnt_init(&cnt, "STDIN");
    processInput(&cnt, stdin);
    cnt_print(&cnt);
}

Count* processInput(Count* this, FILE* f) {
    const unsigned MAXLEN = 1024;
    char           line[MAXLEN];
    while (fgets(line, MAXLEN, f) != NULL) {
        cnt_add(this, line);
    }
    return this;
}

void processFiles(int numArgs, char** args) {
    Count total;
    cnt_init(&total, "TOTAL");

    for (int k = 1; k < numArgs; ++k) {
        processFile(args[k], &total);
    }

    if (numArgs > 2) {
        printf("--------------------------------\n");
        cnt_print(&total);
    }
}

void processFile(char* filename, Count* total) {
    FILE* file = fopen(filename, "r");
    if (!file) {
        fprintf(stderr, "cannot open '%s'\n", filename);
        exit(1);
    }

    Count cnt;
    cnt_init(&cnt, filename);
    processInput(&cnt, file);
    cnt_print(&cnt);

    cnt_sum(total, &cnt);
}
