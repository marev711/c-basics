#include <stdio.h>
#include <time.h>
#include <stdlib.h>

typedef unsigned long long XXL;

XXL fibonacci(unsigned n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fibonacci(n - 1) + fibonacci(n - 2);
}

int main(int argc, char** argv) {
    const unsigned n = (unsigned) (argc == 1 ? 10 : atoi(argv[1]));

    clock_t start  = clock();
    XXL     result = fibonacci(n);
    clock_t end    = clock();

    double elapsedSeconds = (end - start) / (double)CLOCKS_PER_SEC;
    printf("fibonacci(%d) = %lld (elapsed %.4f secs)\n", n, result, elapsedSeconds);

    return 0;
}


