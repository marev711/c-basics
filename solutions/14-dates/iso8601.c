#include <stdio.h>
#include <time.h>
#include <assert.h>

int main() {
    time_t now = time(NULL);

    struct tm tmBuf;
    const struct tm* datetime = localtime_r(&now, &tmBuf);
    assert(datetime == &tmBuf);

    char buf[128];
    strftime(buf, sizeof(buf), "%F %T", datetime);
    printf("today: %s\n", buf);

    return 0;
}
