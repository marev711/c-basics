#!/usr/bin/env bash
set -eu

STD="-std=c11"
WARN="-Wall -Wextra -Werror -Wfatal-errors"
SRC=math-app.c
APP=math-app
LIB=-lm

set -x
gcc ${STD} ${WARN} -save-temps ${SRC} -o ${APP} ${LIB}
gcc ${STD} ${WARN} -static ${SRC} -o ${APP}-static ${LIB}

size ${APP} ${APP}-static

nm ${APP} | grep ' U '
(nm ${APP}-static | grep ' U ') || true

./$APP 7 3
./${APP}-static 5 5
