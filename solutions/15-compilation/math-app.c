#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char** argv) {
    unsigned n = 10;
    unsigned e = 3;

    if (argc > 1) n = (unsigned)atoi(argv[1]);
    if (argc > 2) e = (unsigned)atoi(argv[2]);

    printf("pow(%d, %d) = %.3lf\n", n, e, pow(n, e));
    return 0;
}

