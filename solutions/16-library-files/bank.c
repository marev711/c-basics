#include <stdio.h>
#include <assert.h>
#include "Account.h"
#include "List.h"
#include "bank-support.h"


int main(int numArgs, char* args[]) {
    int numAccounts = (numArgs > 1) ? atoi(args[1]) : 5;

    List accounts;
    list_init(&accounts);
    populate(&accounts, numAccounts);
    assert(list_size(&accounts) == numAccounts);
    print(&accounts);

    printf("------------\n");
    apply(&accounts, &account_final_amount);
    print(&accounts);

    printf("------------\n");
    apply(&accounts, &account_initial_amount);
    print(&accounts);

    drain(&accounts);
    assert(list_empty(&accounts));

    printf("------------\n");
    Account* ptr = account_new(100, 1.5);
    account_print(ptr);
    account_dispose(ptr);

    return 0;
}
