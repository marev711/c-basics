#pragma once

#include <stdlib.h>
#include "Node.h"

typedef struct {
    Node* first;
} List;


extern List*    list_init(List* this);
extern List*    list_new();
extern void     list_dispose(List* this);
extern int      list_empty(List* this);
extern int      list_size(List* this);
extern List*    list_push(List* this, Account* data);
extern Account* list_pop(List* this);
