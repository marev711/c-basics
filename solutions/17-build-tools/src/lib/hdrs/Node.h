#pragma once

#include "Account.h"

typedef struct sNode {
    Account     * data;
    struct sNode* next;
} Node;

extern Node* node_new(Account* data, Node* next);
extern void node_dispose(Node* this);
