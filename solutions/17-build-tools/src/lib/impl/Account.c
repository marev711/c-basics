#include <stdio.h>
#include <stdlib.h>
#include "Account.h"


static char* account_type(AccountType type) {
    switch (type) {
        case CHECK:
            return "CHECK ";
        case CREDIT:
            return "CREDIT";
    }
    return "**INVALID**";
}

static Account* account_alloc() {
    return (Account*) calloc(1, sizeof(Account));
}

static Account* account_init(Account* this, AccountType type, Balance amount, InterestRate percentage) {
    this->type    = type;
    this->balance = amount;
    this->rate    = percentage;
    return this;
}

Account* account_new(Balance amount, InterestRate percentage) {
    static int nextType = 0;
    return account_init(account_alloc(), nextType++ % 2 == 0 ? CHECK : CREDIT, amount, percentage);
}

void account_dispose(Account* this) {
    free((void*) this);
}

Account* account_print(Account* this) {
    printf("account{%s: SEK %5d, %.2f%%}\n",
           account_type(this->type), this->balance, this->rate);
    return this;
}

Balance account_final_amount(Account* this) {
    const Balance      a = this->balance;
    const InterestRate p = this->rate;
    return (Balance) (a * (1 + p / 100));
}

Balance account_initial_amount(Account* this) {
    const Balance      a = this->balance;
    const InterestRate p = this->rate;
    return (Balance) (a / (1 + p / 100));
}
