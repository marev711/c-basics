#include "List.h"

static List* list_alloc() {
    return (List*) calloc(1, sizeof(List));
}

List* list_init(List* this) {
    this->first = NULL;
    return this;
}

List* list_new() {
    return list_init(list_alloc());
}

void list_dispose(List* this) {
    free((void*) this);
}

int list_empty(List* this) {
    return this->first == NULL;
}

static int list_size_compute(Node* node, int size) {
    if (node==NULL) return size;
    return list_size_compute(node->next, size + 1);
}

int list_size(List* this) {
    return list_size_compute(this->first, 0);
}

List* list_push(List* this, Account* data) {
    this->first = node_new(data, this->first);
    return this;
}

Account* list_pop(List* this) {
    if (list_empty(this)) return NULL;

    Node* n = this->first;
    this->first = n->next;
    Account* data = n->data;
    node_dispose(n);

    return data;
}

