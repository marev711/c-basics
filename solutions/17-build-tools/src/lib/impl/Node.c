#include <stdlib.h>
#include "Node.h"

static Node* node_alloc() {
    return (Node*) calloc(1, sizeof(Node));
}

static Node* node_init(Node* this, Account* data, Node* next) {
    this->data = data;
    this->next = next;
    return this;
}

Node* node_new(Account* data, Node* next) {
    return node_init(node_alloc(), data, next);
}

void node_dispose(Node* this) {
    free((void*) this);
}

