cmake_minimum_required(VERSION 3.8)
project(tst_cunit)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "-g -Wall -Wextra -Werror -Wfatal-errors")

add_executable(tst_cunit
        numbers.h numbers.c
        test-driver.c
        )
target_link_libraries(tst_cunit cunit)

