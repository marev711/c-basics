#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include "numbers.h"

void test_sum() {
    CU_ASSERT_EQUAL(sum(10), 55);
    CU_ASSERT_EQUAL(sum(100), 5050);
}

void test_fib() {
    CU_ASSERT_EQUAL(fib(10), 55);
    CU_ASSERT_EQUAL(fib(42), 267914296);
}

void test_factorial() {
    CU_ASSERT_EQUAL(factorial(-1), 0);
    CU_ASSERT_EQUAL(factorial(1), 1);
    CU_ASSERT_EQUAL(factorial(5), 120);
}


int main() {
    if (CU_initialize_registry() != CUE_SUCCESS)
        return CU_get_error();

    CU_pSuite suite = CU_add_suite("Numbers-Test", NULL, NULL);
    CU_add_test(suite, "sum(int)", &test_sum);
    CU_add_test(suite, "fac(int)", &test_factorial);
    CU_add_test(suite, "fib(int)", &test_fib);

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}
